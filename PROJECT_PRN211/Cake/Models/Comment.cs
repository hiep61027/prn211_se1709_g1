﻿using System;
using System.Collections.Generic;

namespace Cake.Models
{
    public partial class Comment
    {
        public int CommentId { get; set; }
        public string? Description { get; set; }
        public int? BlogId { get; set; }
        public int? Owner { get; set; }

        public virtual Blog? Blog { get; set; }
        public virtual User? OwnerNavigation { get; set; }
    }
}
