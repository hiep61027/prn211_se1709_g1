﻿using System;
using System.Collections.Generic;

namespace Cake.Models
{
    public partial class User
    {
        public User()
        {
            Blogs = new HashSet<Blog>();
            Comments = new HashSet<Comment>();
            Feedbacks = new HashSet<Feedback>();
            Orders = new HashSet<Order>();
            Roles = new HashSet<Role>();
        }

        public int UserId { get; set; }
        public string? UserName { get; set; }
        public string? Password { get; set; }
        public string? Name { get; set; }
        public string? Phone { get; set; }
        public string? Mail { get; set; }
        public bool? Sex { get; set; }
        public DateTime? Dob { get; set; }
        public string? Avatar { get; set; }
        public bool? IsActive { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
    }
}
