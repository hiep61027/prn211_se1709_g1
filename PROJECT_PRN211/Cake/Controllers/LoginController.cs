﻿using Cake.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Web;

namespace Cake.Controllers
{
	public class LoginController : Controller
	{
		private readonly IHttpContextAccessor _contextSession;
		
		private readonly BakeryCakeContext _context;
//, IHttpContextAccessor httpContextAccessor
		public LoginController(BakeryCakeContext context)
		{
			_context = context;
			//_contextSession = httpContextAccessor;
		}
		public IHttpContextAccessor HttpContextAccessor { get; }
		public IActionResult Index()
		{
			return View();
		}
		public async Task<IActionResult> Login(string username, string pass)
		{
			var member1 = _context.Users
				.Include(x => x.Roles)
				.SingleOrDefault(u => u.UserName == username && u.Password == pass);
			if (member1 != null)
			{
				//_contextSession.HttpContext.Session.SetString("account", JsonConvert.SerializeObject(member1));
				return RedirectToAction("Index", "Home");
			}
			else
			{
				ViewData["fail"] = "Wrong username or pass";
				return View("Index");
			}
		}
		public IActionResult Email()
		{
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Email(string mail, string pass)
		{
			var member1 = _context.Users
				.Include(x => x.Roles)
				.SingleOrDefault(u => u.Mail == mail && u.Password == pass);
			if (member1 != null)
			{
				//_contextSession.HttpContext.Session.SetString("account", JsonConvert.SerializeObject(member1));
				return RedirectToAction("Index", "Home");
			}
			else
			{
				ViewData["fail"] = "Wrong email or pass";
				return View();
			}
		}
		public IActionResult Phone()
		{
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Phone(string phone, string pass)
		{
			var member1 = _context.Users
				.Include(x => x.Roles)
				.SingleOrDefault(u => u.Phone == phone && u.Password == pass);
			if (member1 != null)
			{
				//_contextSession.HttpContext.Session.SetString("account", JsonConvert.SerializeObject(member1));
				return RedirectToAction("Index", "Home");
			}
			else
			{
				ViewData["fail"] = "Wrong username or pass";
				return View("Index");
			}
		}
	}
}
